class tests_options:
    def decor_for_head(self, text):
        length = int(len(text))
        lst_of_simbols = []
        for i in range(length + 4):
            lst_of_simbols.append("=")

        print("\n")
        print(("").join(lst_of_simbols))
        print("==" + str(text) + "==")
        print(("").join(lst_of_simbols))
        print("\n")

    def __init__(self, file):
        self.file = file
        self.__themes = list(file)

    def themes(self, name="РАЗДЕЛ ПРОСМОТРА ТЕМ ТЕСТОВ"):
        per = 0
        self.decor_for_head(name)
        imp = input("Введите тип вывода тем тестов: ")
        try:
            int(imp)
        except:
            if imp == "prettify":
                for theme in self.__themes:
                    per += 1
                    print(str(per) + " - " + theme)
            elif imp == "list":
                print(self.__themes)
            else:
                print("Некорректный ввод")
                self.themes()
        else:
            print("Некорректный ввод")
            self.themes()

    def questions(self):
        self.decor_for_head("РАЗДЕЛ ПРОСМОТРА ВОПРОСОВ")
        quality = input("Введите тип вывода вопросов: ")
        inp = input("Введите номер темы: ")
        try:
            int(inp)
        except:
            print("Некорректный ввод номера темы")
            self.questions()
        else:
            if int(inp) <= int(len(self.__themes)):
                theme = str(self.__themes[int(inp)])
            else:
                print("Некорректный ввод номера темы")
                self.questions()

        try:
            int(quality)
        except:
            if quality == "prettify":
                per = 0
                for i in list(self.file.get(theme).keys()):
                    per += 1
                    i = i.replace("@#@", "\"")
                    print(str(per) + " - " + str(i))
            elif quality == "list":
                print(list(self.file.get(theme).keys()))
            else:
                print("Некорректный ввод типа вывода вопросов")
                self.questions()
        else:
            print("Некорректный ввод типа выводов вопроса")
            self.questions()

    def questions_changer(self):
        self.decor_for_head("РАЗДЕЛ ИЗМЕНЕНИЯ СЛОВАРЯ ВОПРОСОВ")
        inp = input("Введите номер темы: ")
        if str(inp) == "?":
            self.themes("Просмотр тем")
        try:
            int(inp)
        except:
            print("Некорректный ввод номера темы")
            self.questions_changer()
        else:
            if int(inp) <= int(len(self.__themes)):
                theme = str(self.__themes[int(inp)])
            else:
                print("Некорректный ввод номера темы")
                self.questions_changer()
        input_question = input(
            "Вставьте новый словарь с вопросами для темы \"" + str(theme) + "\" в правильном формате: ")
        if str(type(input_question)) == "<class 'dict'>":
            lst_of_new_values = list(input_question.values())
            per = 0
            for i in lst_of_new_values:
                per += 1
                if not str(type(i)) == "<class 'dict'>":
                    end_of_per = list(per)[-1]
                    if end_of_per in [1, 4, 5, 9, 0]:
                        post = "-ый"
                    elif end_of_per in [2, 6, 7, 8]:
                        post = "-ой"
                    elif end_of_per in [3]:
                        post = "-ий"
                    print("Неправильный формат ввода. " + str(
        per) + str(post) + "вопрос или его значение не соответствуют формату (" + str(i) + ")")

            self.file_changed_questions = self.file
            self.file_changed_questions[str(theme)] = input_question
            print("Новый словарь с тестами готов")
        else:
            print("Неправильный формат введенной информации, ее тип: " + str(type(input_question)))
