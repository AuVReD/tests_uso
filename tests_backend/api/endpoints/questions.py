import logging

from flask import request
from flask_restplus import Resource
from tests_backend.api.serializers import themes
from tests_backend.api.restplus import api
from tests_backend.models import Question

log = logging.getLogger(__name__)

questions_route = api.namespace('questions', description='themes operations')


@questions_route.route('/')
class QuestionsCollection(Resource):

    def get(self):
        questions = Question.query.first()
        return questions.as_dict_glob()

#     @api.response(201, 'Theme successfully created.')
#     @api.expect(themes)
#     def post(self):
#         data = request.json
#         Theme(name=data['name'], url=data['url']).save()
#         return None, 201
#
#
# @questions_route.route('/<int:id>')
# @api.response(404, 'Theme not found.')
# class ThemeItem(Resource):
#
#     def get(self, id):
#         theme = Theme.query.filter(Theme.id == id).one()
#         return theme.as_dict()
#
#
# #
# @ns.route('/<int:id>')
# @api.response(404, 'Category not found.')
# class CategoryItem(Resource):
#
#     @api.marshal_with(category_with_posts)
#     def get(self, id):
#         """
#         Returns a category with a list of posts.
#         """
#         return Category.query.filter(Category.id == id).one()
#
#     @api.expect(category)
#     @api.response(204, 'Category successfully updated.')
#     def put(self, id):
#         """
#         Updates a blog category.
#
#         Use this method to change the name of a blog category.
#
#         * Send a JSON object with the new name in the request body.
#
#         ```
#         {
#           "name": "New Category Name"
#         }
#         ```
#
#         * Specify the ID of the category to modify in the request URL path.
#         """
#         data = request.json
#         update_category(id, data)
#         return None, 204
#
#     @api.response(204, 'Category successfully deleted.')
#     def delete(self, id):
#         """
#         Deletes blog category.
#         """
#         delete_category(id)
#         return None, 204
