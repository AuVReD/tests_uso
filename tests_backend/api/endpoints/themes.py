import logging

from sqlalchemy import and_
from flask_restplus import Resource
from tests_backend.api.serializers import themes
from tests_backend.api.restplus import api
from tests_backend.models import Theme, Question, Answer

log = logging.getLogger(__name__)

themes_route = api.namespace('themes', description='themes operations')


@themes_route.route('/')
class ThemesCollection(Resource):

    @api.marshal_list_with(themes)
    def get(self):
        themes = Theme.query.all()
        return themes


@themes_route.route('/<int:theme_id>')
@api.response(404, 'Theme not found.')
class ThemeItem(Resource):

    def get(self, theme_id):
        theme = Theme.query.filter(Theme.id == theme_id).one()
        return theme.as_dict()\


@themes_route.route('/exam/<int:theme_id>')
@api.response(404, 'Theme not found.')
class ExamQuestions(Resource):

    def get(self, theme_id):
        theme_res = Theme.query.filter(Theme.id == theme_id).first()
        theme = theme_res.as_dict()
        questions = Question.select_for_exam(theme_id)
        theme['questions'] = questions
        theme['questions_amount'] = 30
        return theme



@themes_route.route('/<int:theme_id>/questions')
class QuestionsCollection(Resource):

    def get(self, theme_id):
        theme = Theme.query.filter(Theme.id == theme_id).one()
        return [q.as_dict_glob() for q in theme.questions]


@themes_route.route('/<int:theme_id>/questions/<int:question_id>')
class QuestionItem(Resource):

    def get(self, theme_id, question_id):
        question = Question.query.filter(and_(Question.theme_id == theme_id),
                                          (Question.in_theme_id == question_id)).one()
        return question.as_dict()

@themes_route.route('/<int:theme_id>/questions/<int:question_id>/answers')
class AnswersCollection(Resource):

    def get(self, theme_id, question_id):
        question = Question.query.filter(and_(Question.theme_id == theme_id),
                                          (Question.in_theme_id == question_id)).one()
        return [a.as_dict_glob() for a in question.answers]

@themes_route.route('/<int:theme_id>/questions/<int:question_id>/answers/<int:answer_id>')
class AnswerItem(Resource):

    def get(self, theme_id, question_id, answer_id):
        theme = Theme.query.filter(Theme.id == theme_id).one()
        answer = Answer.query.filter(and_(Answer.question_id == question_id),
                                          (Answer.in_question_id == answer_id)).one()
        return answer.as_dict_glob()

#
# @ns.route('/<int:id>')
# @api.response(404, 'Category not found.')
# class CategoryItem(Resource):
#
#     @api.marshal_with(category_with_posts)
#     def get(self, id):
#         """
#         Returns a category with a list of posts.
#         """
#         return Category.query.filter(Category.id == id).one()
#
#     @api.expect(category)
#     @api.response(204, 'Category successfully updated.')
#     def put(self, id):
#         """
#         Updates a blog category.
#
#         Use this method to change the name of a blog category.
#
#         * Send a JSON object with the new name in the request body.
#
#         ```
#         {
#           "name": "New Category Name"
#         }
#         ```
#
#         * Specify the ID of the category to modify in the request URL path.
#         """
#         data = request.json
#         update_category(id, data)
#         return None, 204
#
#     @api.response(204, 'Category successfully deleted.')
#     def delete(self, id):
#         """
#         Deletes blog category.
#         """
#         delete_category(id)
#         return None, 204
