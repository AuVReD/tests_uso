import "./custom.js";

$(document).ready(function () {
/////////////////////////////////////////////////////////////
    var headThemeBox = $("#head-theme");//                 //
    var headThemeSpan = $("#head-theme span");             //
    var navigate = $(".navigation");    //                 //
    var dropMenu = $(".drop-menu");     //                 //
                                        //                 //
    var searchForm = "";                //                 //
    var buttonClear = "";               //                 //
    var buttonSearch = "";              //                 //

    var search = "";
    var menuListButton = "";
    var themeChangeButton = "";
    var variantOfTestingChangeButton = "";

    var onClickDate = "";

    var forwBackButts = "";
    var forwardButton = "";
    var backwardsButton = "";
    var endExamButton = "";
    var questionCounter = "";
    var inputDict = "";
    var answers = [];
    var values = [];
    var dictOfQuest = "";
    var vals = [];
    var valsForButton = [];
    var resultsOfAnswers = {
        1: 0,
        2: 0,
        3: 0,
        4: 0,
        5: 0,
        6: 0,
        7: 0,
        8: 0,
        9: 0,
        10: 0,
        11: 0,
        12: 0,
        13: 0,
        14: 0,
        15: 0,
        16: 0,
        17: 0,
        18: 0,
        19: 0,
        20: 0,
        21: 0,
        22: 0,
        23: 0,
        24: 0,
        25: 0,
        26: 0,
        27: 0,
        28: 0,
        29: 0,
        30: 0
    };
    var variant = "";
    var trueAnswers = {1: [],
        2: [],
        3: [],
        4: [],
        5: [],
        6: [],
        7: [],
        8: [],
        9: [],
        10: [],
        11: [],
        12: [],
        13: [],
        14: [],
        15: [],
        16: [],
        17: [],
        18: [],
        19: [],
        20: [],
        21: [],
        22: [],
        23: [],
        24: [],
        25: [],
        26: [],
        27: [],
        28: [],
        29: [],
        30: []};

    var date = "";
    var dateQ = "";
    var beginTime = "";
    var endTime = "";

    var apiUrl = "http://localhost:5000/api/";
    //                 //
    //    Начальные    //
    var mainBody = $("#main-body");
    var row = $("#row");                //      бинды      //
    var mainRow = "";
    var navRow = $("#nav-row");         //                 //

/////////////////////////////////////////////////////////////

    var boxOf = function (element, blockType) {
        if (blockType == 'block') {
            return $('<div class="col-sm-6 col-lg-4 p-0"><div class="block box m-1 p-2 d-flex justify-content-center flex-column">' +
                '<p class="m-0">' + element + '</p></div></div>');
        } else if (blockType == 'block-question') {
            return $('<div class="col-12 col-lg-6 p-0">' +
                '<div class="block-question box m-1 d-flex justify-content-center flex-column">' +
                '<p class="m-1">' + element + '</p></div></div>');
        }
    };

    var themeChangeButtonFunc = function (button, forwBackButtsCounter) {
        button.on("click", function () {
            if (forwBackButtsCounter == 1) {
                forwBackButts.remove();
                mainRow.remove();
            }

            dropMenu.empty();
            menuListButton.parent().remove();
            homePage();
        });
    };
    var variantOfTestingChangeButtonFunc = function (button, numOfTheme, forwBackButtsCounter) {
        button.on("click", function () {
            dropMenu.empty();
            menuListButton.parent().remove();
            if (forwBackButtsCounter == 1) {
                forwBackButts.remove();
                mainRow.remove();
            }
            variantsOfContinue(1, numOfTheme);
        });
    };

    var breadcrumbOfNumberOfQuestion = function (input, type) {
        if (type == "test") {
            row.empty();
            $.ajax({
                type: 'GET',
                url: apiUrl + 'themes/' + (parseInt(input) + 1).toString() + "/questions",
                dataType: 'json',
                success: function (data) {
                    row.empty();
                    row.prepend('<div class="col-2"><p class="m-0">' + questionCounter + '/' + data.length + '</p></div>');
                }
            });

        } else {
            row.empty();
            row.prepend('<div class="col-2"><p>' + questionCounter + '/' + input + '</p></div>');
        }
    };

    var createSearchBar = function (typeOfInfo, numOfTheme) {
        dropMenu.prepend('<div class="input-group mx-auto" id="search"><input class="form-control" id="searchStr" aria-label="Search" autocomplete="off"><div class="input-group-append"><button class="btn btn-search btn-searching" id="searchb" type="button">Поиск</button><button class="btn btn-search btn-searching" id="reset" type="button">Сброс</button></div></div>');

        search = $("#search");
        buttonSearch = $("#searchb");
        buttonClear = $("#reset");
        searchForm = $("#searchStr");

        if (typeOfInfo == "theme") {
            searchForm.attr("placeholder", "Тема...");
            searchBtnFunc(themesFunc(), themesUpFunc(), 'block');
            clearBtnFunc(themesFunc(), 'block');
        } else {
            search.removeClass("mx-auto").addClass("mr-auto").addClass("mt-2");
            searchForm.attr("placeholder", "Вопрос...");
            searchBtnFunc(questionsFunc(numOfTheme), questionsUpFunc(numOfTheme), 'block-question');
            clearBtnFunc(questionsFunc(numOfTheme), 'block-question');
        }
    };

////////////////////////////////////////////////////////////////////////////////////////////////////
    var headline = function (content) {
        row.append('<div class="col-12 p-0">' +
            '<div class="block-theme box m-1 d-flex justify-content-center flex-column">' +
            '<p class="m-0 headline-content">' + content + '</p></div></div>');
        if ($(".headline-content").height() > 60) {
            $(".block-theme").height($(".headline-content").height());

            // breadcrumb.css("height", )
        } else {
            $(".block-theme").height(60);
        }
    };                                          //          //
    //          //
    var homePage = function () {
        //////////////////////////////////////////
        // Аргументы:                           //
        //  1)список с загл. буквой             //
        //  2)тип блока для элемента:           //
        //     а)block - для тем                //
        //     б)block-question - для вопросов  //
        //////////////////////////////////////////
        mainBody.css("overflow", "unset");
        headThemeBox.css("text-align", "center");
        navRow.addClass("mb-2");

        //Создание поисковой строки и верхнего меню//
        dropMenu.empty();
        dropMenu.show();



        headThemeSpan.text('Выбор темы');

        createSearchBar("theme");

        //Создание страницы с темами//
        row.empty();

        $.ajax({
            type: 'GET',
            url: apiUrl + 'themes',
            dataType: 'json',
            success: function (data) {
                for (let i in data) {
                    let activeChildTile = boxOf(data[i]['name'][0].toUpperCase() + data[i]['name'].slice(1), 'block');
                    variantsOfContinue(activeChildTile, i);
                    row.append(activeChildTile);
                }
            }
        });


    };                                                 // Создание //
    var variantsOfContinue = function (clickedBlock, numOfClickedTheme) {
        mainBody.css("overflow", "unset");
        if (clickedBlock == 1) {
            navRow.prepend('<div class="col-2"><button class="navbar-toggler btn-search" id="menu-btn" type="button"><i class="fa fa-bars"></i></button></div>');
            menuListButton = $("#menu-btn");
            let theme = "";
            $.ajax({
                type: 'GET',
                url: apiUrl + 'themes',
                dataType: 'json',
                success: function (data) {
                    theme = data[parseInt(numOfClickedTheme)]['name'];
                }
            });
            headThemeSpan.text('Тема: ' + theme);
            // headThemeSpan.css("margin-top", ((navRow.height() - headThemeSpan.height()) / 2));
            // menuListButton.css("margin-top", Math.round((navRow.height() - 35) / 2));

            navRow.removeClass("mb-2");

            headThemeBox.css("text-align", "end");

            dropMenu.empty();
            dropMenu.hide();
            // dropMenu.height(0);
            dropMenu.append('<div class="drop-menu-item col-12 px-0 mt-1" id="theme-change-button"><p class="m-0">Сменить тему</p></div>');
            themeChangeButton = $("#theme-change-button");
            themeChangeButtonFunc(themeChangeButton, 0);

            menuListButton.show();
            menuListButton.on("click", function () {
                this.blur();
                dropMenu.toggle();
                // dropMenu.animate({height : themeChangeButton.height()}, 1000);
            });

            row.empty();
            searchForm.val("");
            row.append('<div class="col-12 col-md-6 p-0" id="exam">' +
                '<div class="block box m-1 d-flex justify-content-center flex-column">' +
                '<p class="m-0">Экзамен</p></div></div>');
            row.append('<div class="col-12 col-md-6 p-0" id="show-answers">' +
                '<div class="block box m-1 d-flex justify-content-center flex-column">' +
                '<p class="m-0">Всегда показывать ответы</p></div></div>');
            row.append('<div class="col-12 col-md-6 p-0" id="test-with-answers">' +
                '<div class="block box m-1 d-flex justify-content-center flex-column">' +
                '<p class="m-0">Тест с ответами</p></div></div>');
            row.append('<div class="col-12 col-md-6 p-0" id="lst-of-questions">' +
                '<div class="block box m-1 d-flex justify-content-center flex-column">' +
                '<p class="m-0">Список вопросов</p></div></div>');

            $("#lst-of-questions").on("click", function () {
                dropMenu.hide();
                questionsPage(numOfClickedTheme);
            });
            $("#show-answers").on("click", function () {
                dropMenu.hide();
                questionsWithAnswersPage(numOfClickedTheme, 1);
            });
            $("#test-with-answers").on("click", function () {
                dropMenu.hide();
                testWithAnswersPage(numOfClickedTheme);
            });
            $("#exam").on("click", function () {
                dropMenu.hide();
                examPage(numOfClickedTheme);
            });
        } else {
            clickedBlock.on("click", function () {
                navRow.prepend('<div class="col-2"><button class="navbar-toggler btn-search" id="menu-btn" type="button"><i class="fa fa-bars"></i></button></div>');
                menuListButton = $("#menu-btn");
                let theme = "";
                $.ajax({
                    type: 'GET',
                    url: apiUrl + 'themes',
                    dataType: 'json',
                    success: function (data) {
                        theme = data[parseInt(numOfClickedTheme)]['name'];
                        headThemeSpan.text('Тема: ' + theme);
                    }
                });


                navRow.removeClass("mb-2");

                headThemeBox.css("text-align", "end");

                dropMenu.empty();
                dropMenu.hide();
                // dropMenu.height(0);
                dropMenu.append('<div class="drop-menu-item col-12 px-0 mt-1" id="theme-change-button"><p class="m-0">Сменить тему</p></div>');
                themeChangeButton = $("#theme-change-button");
                themeChangeButtonFunc(themeChangeButton, 0);

                menuListButton.show();
                menuListButton.on("click", function () {
                    this.blur();
                    dropMenu.toggle();
                });


                row.empty();
                searchForm.val("");
                row.append('<div class="col-12 col-md-6 p-0" id="exam">' +
                    '<div class="block box m-1 d-flex justify-content-center flex-column">' +
                    '<p class="m-0">Экзамен</p></div></div>');
                row.append('<div class="col-12 col-md-6 p-0" id="show-answers">' +
                    '<div class="block box m-1 d-flex justify-content-center flex-column">' +
                    '<p class="m-0">Всегда показывать ответы</p></div></div>');
                row.append('<div class="col-12 col-md-6 p-0" id="test-with-answers">' +
                    '<div class="block box m-1 d-flex justify-content-center flex-column">' +
                    '<p class="m-0">Тест с ответами</p></div></div>');
                row.append('<div class="col-12 col-md-6 p-0" id="lst-of-questions">' +
                    '<div class="block box m-1 d-flex justify-content-center flex-column">' +
                    '<p class="m-0">Список вопросов</p></div></div>');

                $("#lst-of-questions").on("click", function () {
                    dropMenu.hide();
                    questionsPage(numOfClickedTheme);
                });
                $("#show-answers").on("click", function () {
                    dropMenu.hide();
                    questionsWithAnswersPage(numOfClickedTheme, 1);
                });
                $("#test-with-answers").on("click", function () {
                    dropMenu.hide();
                    testWithAnswersPage(numOfClickedTheme);
                });
                $("#exam").on("click", function () {
                    dropMenu.hide();
                    examPage(numOfClickedTheme);
                });
            });
        }
    };        //  страниц //
    var questionsPage = function (numOfTheme) {
        row.empty();
        dropMenu.hide();
        dropMenu.append('<div class="drop-menu-item col-12 px-0 mt-1" id="variant-of-testing-change-button"><p class="m-0">Сменить способ тестирования</p></div>');
        variantOfTestingChangeButton = $("#variant-of-testing-change-button");
        variantOfTestingChangeButtonFunc(variantOfTestingChangeButton, numOfTheme, 0);
        let theme = "";
        $.ajax({
            type: 'GET',
            url: apiUrl + 'themes',
            dataType: 'json',
            success: function (data) {
                theme = data[parseInt(numOfTheme)]['name'];
                headThemeSpan.text('Тема: ' + theme + '. Список вопросов');
            }
        });
        $.ajax({
            type: 'GET',
            url: apiUrl + 'themes/' + (parseInt(numOfTheme) + 1).toString() + "/questions",
            dataType: 'json',
            success: function (data) {
                for(let i in data){
                    let activeChildTileQuest = boxOf(data[i]['name'], 'block-question');
                    activeChildTileQuest.on("click", function () {
                        questionsWithAnswersPage(numOfTheme, parseInt(i) + 1);
                    });
                    row.append(activeChildTileQuest);
                    // normQuests.push(data[i]['name']);
                }
            }
        });


        createSearchBar("question", numOfTheme);

        for (let question in questionsFunc(numOfTheme)) {
            let activeChildTileQuest = boxOf(questionsFunc(numOfTheme)[question], 'block-question');
            activeChildTileQuest.on("click", function () {
                questionsWithAnswersPage(numOfTheme, parseInt(question) + 1);
            });
            row.append(activeChildTileQuest);
        }
    };                                  //          //
    var questionsWithAnswersPage = function (numOfTheme, numOfQuestion) {
        //Формирование страницы//
        row.empty();
        dropMenu.hide();
        $("body").append('<div class="row mx-0" id="butts"></div>');
        forwBackButts = $("#butts");
        mainBody.append('<div class="row mx-0" id="main-row"></div>');
        mainRow = $("#main-row");
        mainBody.css({"overflow" : "scroll", "height" : "calc(100vh - 190px)"});


        //Создание кнопок вперед и назад//
        forwBackButts.append('<div class="col-6"><button type="button" class="btn btn-search btn-forwback" id="btn-back">Назад</button></div>');
        forwBackButts.append('<div class="col-6"><button type="button" class="btn btn-search btn-forwback clickble" id="btn-forward">Вперед</button></div>');
        backwardsButton = $("#btn-back");
        forwardButton = $("#btn-forward");

        //Создание среды в верхнем меню//
        dropMenu.empty();
        let theme = "";
        $.ajax({
            type: 'GET',
            url: apiUrl + 'themes',
            dataType: 'json',
            success: function (data) {
                theme = data[numOfTheme]['name'];
                headThemeSpan.text('Тема: ' + theme + '. Всегда показывать ответы');
            }
        });

        dropMenu.append('<div class="drop-menu-item col-12 px-0 mt-1" id="theme-change-button"><p class="m-0">Сменить тему</p></div>');
        themeChangeButton = $("#theme-change-button");
        themeChangeButtonFunc(themeChangeButton, 1);
        dropMenu.append('<div class="drop-menu-item col-12 px-0 mt-1" id="variant-of-testing-change-button"><p class="m-0">Сменить способ тестирования</p></div>');
        variantOfTestingChangeButton = $("#variant-of-testing-change-button");
        variantOfTestingChangeButtonFunc(variantOfTestingChangeButton, numOfTheme, 1);

        //##//Основная работа страницы//##//
        questionCounter = numOfQuestion;
        mainRow.empty();
        breadcrumbOfNumberOfQuestion(numOfTheme, "test");


        $.ajax({
            type: 'GET',
            url: apiUrl + 'themes/' + (parseInt(numOfTheme) + 1).toString() + "/questions/" + questionCounter.toString(),
            dataType: 'json',
            success: function (data) {
                mainRow.prepend('<div class="col-12 quest-header"><p>' + data['name'] + '</p></div>');
            }
        });

        mainRow.append('<div class="col-12"><ol class="answers-list" onmousedown="return false" onselectstart="return false"></ol></div>');
        answersFunc(numOfTheme, (questionCounter - 1), "show");
        //Работа кнопок вперед назад//
        backwardsButtonFunc(numOfTheme, backwardsButton, "show");
        forwardButtonFunc(numOfTheme, forwardButton, "show");
    };        //          //
    var testWithAnswersPage = function (numOfTheme) {
        //Формирование страницы//
        row.empty();
        dropMenu.hide();
        mainBody.append('<div class="row mx-0" id="butts"></div>');
        forwBackButts = $("#butts");
        mainBody.append('<div class="row mx-0" id="main-row"></div>');
        mainRow = $("#main-row");

        mainBody.css({"overflow" : "scroll", "height" : "calc(100vh - 190px)"});

        //Создание кнопок вперед и назад//
        forwBackButts.append('<div class="col-6"><button type="button" class="btn btn-search btn-forwback" id="btn-back">Назад</button></div>');
        forwBackButts.append('<div class="col-6"><button type="button" class="btn btn-search btn-forwback" id="btn-forward">Вперед</button></div>');
        backwardsButton = $("#btn-back");
        forwardButton = $("#btn-forward");

        //Создание среды в верхнем меню//
        dropMenu.empty();
        let theme = "";
        $.ajax({
            type: 'GET',
            url: apiUrl + 'themes',
            dataType: 'json',
            success: function (data) {
                theme = data[numOfTheme]['name'];
                headThemeSpan.text('Тема: ' + theme + '. Тест с ответами');
            }
        });
        dropMenu.append('<div class="drop-menu-item col-12 px-0 mt-1" id="theme-change-button"><p class="m-0">Сменить тему</p></div>');
        themeChangeButton = $("#theme-change-button");
        themeChangeButtonFunc(themeChangeButton, 1);
        dropMenu.append('<div class="drop-menu-item col-12 px-0 mt-1" id="variant-of-testing-change-button"><p class="m-0">Сменить способ тестирования</p></div>');
        variantOfTestingChangeButton = $("#variant-of-testing-change-button");
        variantOfTestingChangeButtonFunc(variantOfTestingChangeButton, numOfTheme, 1);

        //##//Основная работа страницы//##//
        forwardButton.hide();

        questionCounter = 1;
        mainRow.empty();
        breadcrumbOfNumberOfQuestion(numOfTheme, "test");

        $.ajax({
            type: 'GET',
            url: apiUrl + 'themes/' + (parseInt(numOfTheme) + 1).toString() + "/questions/" + questionCounter.toString(),
            dataType: 'json',
            success: function (data) {
                mainRow.prepend('<div class="col-12 quest-header"><p>' + data['name'] + '</p></div>');
            }
        });
        mainRow.append('<div class="col-12"><ol class="answers-list" onmousedown="return false" onselectstart="return false"></ol></div>');
        answersFunc(numOfTheme, (questionCounter - 1), "hide");
        //Работа кнопок вперед назад//
        backwardsButtonFunc(numOfTheme, backwardsButton, "hide");
        forwardButtonFunc(numOfTheme, forwardButton, "hide");
    };
    var examPage = function (numOfTheme) {
        date = new Date();
        resultsOfAnswers = {1: 0, 2: 0, 3: 0, 4: 0, 5: 0, 6: 0, 7: 0, 8: 0, 9: 0, 10: 0, 11: 0, 12: 0, 13: 0, 14: 0, 15: 0, 16: 0, 17: 0, 18: 0, 19: 0, 20: 0, 21: 0, 22: 0, 23: 0, 24: 0, 25: 0, 26: 0, 27: 0, 28: 0, 29: 0, 30: 0
        };
        //Формирование страницы//
        row.empty();
        dropMenu.hide();
        $("body").append('<div class="row mx-0" id="butts"></div>');
        forwBackButts = $("#butts");
        mainBody.append('<div class="row mx-0" id="main-row"></div>');
        mainRow = $("#main-row");
        mainBody.css({"overflow" : "scroll", "height" : "calc(100vh - 190px)"});


        //Создание кнопок вперед и назад//
        forwBackButts.append('<div class="col-12 mb-2"><button type="button" class="btn btn-search btn-forwback" id="btn-end-exam">Закончить тестирование</button></div>');
        forwBackButts.append('<div class="col-6"><button type="button" class="btn btn-search btn-forwback" id="btn-back">Назад</button></div>');
        forwBackButts.append('<div class="col-6"><button type="button" class="btn btn-search btn-forwback" id="btn-forward">Вперед</button></div>');
        backwardsButton = $("#btn-back");
        forwardButton = $("#btn-forward");
        endExamButton = $("#btn-end-exam");

        //Создание среды в верхнем меню//
        dropMenu.empty();
        let theme = "";
        $.ajax({
            type: 'GET',
            url: apiUrl + 'themes',
            dataType: 'json',
            success: function (data) {
                theme = data[numOfClickedTheme]['name'];
            }
        });
        headThemeSpan.text('Тема: ' + theme + '. Экзамен');
        dropMenu.append('<div class="drop-menu-item col-12 px-0 mt-1" id="theme-change-button"><p class="m-0">Сменить тему</p></div>');
        themeChangeButton = $("#theme-change-button");
        themeChangeButtonFunc(themeChangeButton, 1);
        dropMenu.append('<div class="drop-menu-item col-12 px-0 mt-1" id="variant-of-testing-change-button"><p class="m-0">Сменить способ тестирования</p></div>');
        variantOfTestingChangeButton = $("#variant-of-testing-change-button");
        variantOfTestingChangeButtonFunc(variantOfTestingChangeButton, numOfTheme, 1);

        questionCounter = 1;
        mainRow.empty();
        breadcrumbOfNumberOfQuestion(30, "exam");

        inputDict = questionsRandomFunc(numOfTheme);
        let counter = 0;
        trueAnswers ={1: [],
        2: [],
        3: [],
        4: [],
        5: [],
        6: [],
        7: [],
        8: [],
        9: [],
        10: [],
        11: [],
        12: [],
        13: [],
        14: [],
        15: [],
        16: [],
        17: [],
        18: [],
        19: [],
        20: [],
        21: [],
        22: [],
        23: [],
        24: [],
        25: [],
        26: [],
        27: [],
        28: [],
        29: [],
        30: []};
        for(let item in inputDict[1]){
            counter++;
            for(let i in inputDict[1][item]){
                trueAnswers[counter].push(inputDict[1][item][i]);
            }
        };

        mainRow.append('<div class="col-12 quest-header"><p>' + inputDict[0][questionCounter - 1] + '</p></div>');
        mainRow.append('<div class="col-12"><ol class="answers-list" onmousedown="return false" onselectstart="return false"></ol></div>');
        answersFunc(numOfTheme, (questionCounter - 1), "exam");
        //Работа кнопок вперед назад//
        backwardsButtonFunc(numOfTheme, backwardsButton, "exam");
        forwardButtonFunc(numOfTheme, forwardButton, "exam");
        endExamButtonFunc(numOfTheme, endExamButton);
    };
////////////////////////////////////////////////////////////////////////////////////////////////////
    var themesFunc = function () {
        let normThemes = [];
        $.ajax({
            type: 'GET',
            url: apiUrl + 'themes',
            dataType: 'json',
            success: function (data) {
                for(let i in data){
                    normThemes.push(data[i]['name'][0].toUpperCase() + data[i]['name'].slice(1));
                };
            }
        });
        return normThemes;
    };         //                      //
    // Создание списков тем //
    var themesUpFunc = function () {
        let upThemes = [];
        $.ajax({
            type: 'GET',
            url: apiUrl + 'themes',
            dataType: 'json',
            success: function (data) {
                for(let i in data){
                    upThemes.push(data[i]['name'].toUpperCase());
                };
            }
        });
        // for (let theme in dict) {
        //     upThemes.push(theme.toUpperCase());
        // }
        // ;
        return upThemes;
    };       //                      //
////////////////////////////////////////////////////////////////////////////////
    var questionsFunc = function (numOfTheme) {
        let normQuests = [];
        $.ajax({
            type: 'GET',
            url: apiUrl + 'themes/' + (parseInt(numOfTheme) + 1).toString() + "/questions",
            dataType: 'json',
            success: function (data) {
                for(let i in data){
                    normQuests.push(data[i]['name']);
                }
                return normQuests;
            }
        });
    };      // Создание списков //
    //    вопросов по   //
    var questionsUpFunc = function (numOfTheme) {
        let upQuests = [];
        $.ajax({
            type: 'GET',
            url: apiUrl + 'themes/' + (parseInt(numOfTheme) + 1).toString() + "/questions",
            dataType: 'json',
            success: function (data) {
                for(let i in data){
                    upQuests.push(data[i]['name'].toUpperCase());
                }
            }
        });
        // for (let quest in dict[themes[numOfTheme]]) {
        //     upQuests.push(quest.toUpperCase());
        // }
        // ;
        return upQuests;
    };    //   заданной теме  //
    var questionsRandomFunc = function (numOfTheme) {
        let questNums = [];
        let totalquests = questionsFunc(numOfTheme).length - 1;
        let vr;
        let outNums = [];
        while (totalquests--) {
            questNums.push(totalquests + 1);
        }
        ;
        while (questNums.length) {
            vr = Math.round(Math.random() * (questNums.length - 1));
            outNums.push(questNums[vr]);
            questNums.splice(vr, 1);
        }
        ;
        outNums = outNums.slice(-30);
        let dictForOutput = [[], [], outNums];
        for (let i of outNums) {
            dictForOutput[0].push(questionsFunc(numOfTheme)[i]);
            dictForOutput[1].push(dict[themes[numOfTheme]][(Object.keys(dict[themes[numOfTheme]])[i])]);
        }
        ;
        return dictForOutput;
    };
////////////////////////////////////////////////////////////////////////////////
    var endExamButtonFunc = function(numOfTheme, button){
        button.on("click",function(){
            menuListButton.off("click");
            menuListButton.on("click", function(){
                this.blur();
                dropMenu.toggle();
                mainBody.css({"height" : "calc(100vh - " + $(".navigation").height() + "px - " + forwBackButts.height() + "px)"});
            });
            mainBody.css({"height" : "calc(100vh - " + $(".navigation").height() + "px - " + forwBackButts.height() + "px)"});            let listOfAnswers = [];
            $.each($('.answers-list li'), function (i, v) {
                if($(v).hasClass("neutral-answer")){
                    listOfAnswers.push(1);
                }
                else{
                    listOfAnswers.push(0);
                }
            });
            resultsOfAnswers[questionCounter] = listOfAnswers;

            let timeOfAnswer = Math.floor((new Date() - date) / 1000);
            let timeForOut = "";
            forwBackButts.empty();
            mainRow.empty();
            row.empty();
            if(timeOfAnswer < 60){
                timeForOut = timeOfAnswer + " сек.";
            }
            else if(timeOfAnswer % 60 == 0 && timeOfAnswer < 3600){
                timeForOut = (timeOfAnswer / 60) + " мин.";
            }
            else if(timeOfAnswer > 60 && timeOfAnswer < 3600){
                timeForOut = Math.ceil(timeOfAnswer / 60) + " мин. " + (timeOfAnswer % 60) + " сек.";
            }
            else if(timeOfAnswer > 3600){
                timeForOut = "Вы решали больше часа, хмммммм";
            }
            row.append('<div class="mx-auto">Время решения: ' + timeForOut + '</div>');

            mainRow.append('<div class="col-12 text-center result-exam"><p>Ваш результат: ' + comparisonFunc(trueAnswers, resultsOfAnswers, "dict")[1][0] +'/30<br/><span style="font-size: 20px">Процент правильных ответов: ' + comparisonFunc(trueAnswers, resultsOfAnswers, "dict")[1][1] + '</span></p></div>');

            mainRow.append('<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">\n' +
                '  <div class="modal-dialog" role="document">\n' +
                '    <div class="modal-content">\n' +
                '      <div class="modal-header">\n' +
                '        <h10 class="modal-title" id="exampleModalLabel"></h10>\n' +
                '        <button type="button" class="close" data-dismiss="modal" aria-label="Close">\n' +
                '          <span aria-hidden="true">&times;</span>\n' +
                '        </button>\n' +
                '      </div>\n' +
                '      <div class="modal-body">\n' +
                '      </div>\n' +
                '      <div class="modal-footer">\n' +
                '        <button type="button" class="btn btn-search" data-dismiss="modal">Закрыть</button>\n' +
                '      </div>\n' +
                '    </div>\n' +
                '  </div>\n' +
                '</div>');
            var modal = $(".modal");
            var modalTitle = $(".modal-title");
            var modalBody = $(".modal-body");

            let counter = 0;
            for(let i in inputDict[0]){
                let choice = "";
                if(comparisonFunc(trueAnswers, resultsOfAnswers, "dict")[0][i] == 1){
                    choice = "correct-choice";
                }
                else{
                    choice = "incorrect-choice";
                }
                let tile = $('<div class="row par-answer mb-2"><div class="col-10 my-2 ml-2">' + inputDict[0][i] + '</div><div class="col-1 ml-auto ' + choice + '"></div></div>');

                tile.on("click", function(){
                    modalTitle.text(inputDict[0][i]);
                    modalBody.empty();
                    modalBody.append('<div class="col-12"><ol class="answers-list" onmousedown="return false" onselectstart="return false"></ol></div>');
                    answersFunc(numOfTheme, inputDict[1][i], "showForModal");
                    modal.modal('show');
                });
                mainRow.append(tile);
                counter++;
            }

            forwBackButts.append('<div class="col-12"><button type="button" class="btn btn-search btn-forwback" id="btn-again">Начать экзамен заново</button></div>');

            $("#btn-again").on("click", function(){
                forwBackButts.remove();
                mainRow.remove();
                examPage(numOfTheme);
            });
        });
    };
    var backwardsButtonFunc = function (numOfTheme, button, typeOfVision) {
        button.on("click", function () {
            let listOfAnswers = [];
            $.each($('.answers-list li'), function (i, v) {
                if($(v).hasClass("neutral-answer")){
                    listOfAnswers.push(1);
                }
                else{
                    listOfAnswers.push(0);
                }
            });
            resultsOfAnswers[questionCounter] = listOfAnswers;

            mainRow.empty();
            $(this).blur();
            questionCounter--;
            if (typeOfVision == "show" || typeOfVision == "hide") {
                $.ajax({
                    type: 'GET',
                    url: apiUrl + 'themes/' + (parseInt(numOfTheme) + 1).toString() + "/questions",
                    dataType: 'json',
                    success: function (data) {
                        if (questionCounter <= 0) {
                            questionCounter = data.length + questionCounter;
                        }
                        $.ajax({
                            type: 'GET',
                            url: apiUrl + 'themes/' + (parseInt(numOfTheme) + 1).toString() + "/questions/" + questionCounter.toString(),
                            dataType: 'json',
                            success: function (data) {
                                mainRow.prepend('<div class="col-12 quest-header"><p>' + data['name'] + '</p></div>');
                                if (typeOfVision == "show") {
                                    answersFunc(numOfTheme, (questionCounter - 1), "show");
                                } else if (typeOfVision == "hide") {
                                    forwardButton.hide();
                                    answersFunc(numOfTheme, (questionCounter - 1), "hide");
                                }
                            }
                        });
                    }
                });

                mainRow.append('<div class="col-12"><ol class="answers-list" onmousedown="return false" onselectstart="return false"></ol></div>');
                breadcrumbOfNumberOfQuestion(numOfTheme, "test");
            }
            else if (typeOfVision == "exam") {
                if (questionCounter <= 0) {
                    questionCounter = 30 + questionCounter;
                }
                mainRow.append('<div class="col-12 quest-header"><p>' + inputDict[0][questionCounter - 1] + '</p></div>');
                mainRow.append('<div class="col-12"><ol class="answers-list" onmousedown="return false" onselectstart="return false"></ol></div>');
                answersFunc(numOfTheme, (questionCounter - 1), "exam");
                breadcrumbOfNumberOfQuestion(30, "exam");
                if (resultsOfAnswers[questionCounter] != 0) {
                    $.each(resultsOfAnswers[questionCounter], function (i, v) {
                        if (v == 1){
                            $('.answers-list li').eq(i).addClass("neutral-answer");
                        }
                    });
                }
                // resultsOfAnswers[questionCounter] = valsForButton;
            }
        });
    };
    var forwardButtonFunc = function (numOfTheme, button, typeOfVision) {
        button.on("click", function () {
            if($(this).hasClass("clickble")){
                onClickDate = new Date();
                $(this).removeClass("clickble");
                let listOfAnswers = [];
                $.each($('.answers-list li'), function (i, v) {
                    if ($(v).hasClass("neutral-answer")) {
                        listOfAnswers.push(1);
                    } else {
                        listOfAnswers.push(0);
                    }
                });
                resultsOfAnswers[questionCounter] = listOfAnswers;
                mainRow.empty();
                $(this).blur();
                questionCounter++;
                if (typeOfVision == "show" || typeOfVision == "hide") {
                    $.ajax({
                        type: 'GET',
                        url: apiUrl + 'themes/' + (parseInt(numOfTheme) + 1).toString() + "/questions",
                        dataType: 'json',
                        success: function (data) {
                            mainRow.empty();
                            if (questionCounter >= data.length) {
                                questionCounter = questionCounter - data.length;
                            }
                            $.ajax({
                                type: 'GET',
                                url: apiUrl + 'themes/' + (parseInt(numOfTheme) + 1).toString() + "/questions/" + questionCounter.toString(),
                                dataType: 'json',
                                success: function (dataq) {

                                    mainRow.prepend('<div class="col-12 quest-header"><p>' + dataq['name'] + '</p></div>');
                                    mainRow.append('<div class="col-12"><ol class="answers-list" onmousedown="return false" onselectstart="return false"></ol></div>');

                                    if (typeOfVision == "show") {
                                        answersFunc(numOfTheme, (questionCounter - 1), "show");
                                    } else if (typeOfVision == "hide") {
                                        $(this).hide();
                                        answersFunc(numOfTheme, (questionCounter - 1), "hide");
                                    }
                                }
                            });
                        }
                    });

                    breadcrumbOfNumberOfQuestion(numOfTheme, "test");
                } else if (typeOfVision == "exam") {
                    if (questionCounter > 30) {
                        questionCounter = questionCounter - 30;
                    }
                    mainRow.append('<div class="col-12 quest-header"><p>' + inputDict[0][questionCounter - 1] + '</p></div>');
                    mainRow.append('<div class="col-12"><ol class="answers-list" onmousedown="return false" onselectstart="return false"></ol></div>');
                    answersFunc(numOfTheme, (questionCounter - 1), "exam");
                    breadcrumbOfNumberOfQuestion(30, "exam");


                    if (resultsOfAnswers[questionCounter] != 0) {
                        $.each(resultsOfAnswers[questionCounter], function (i, v) {
                            if (v == 1) {
                                $('.answers-list li').eq(i).addClass("neutral-answer");
                            }
                        });
                    }
                }
            }
            else{
                let nowTime = new Date();
                if((nowTime.getSeconds() - 1) >= onClickDate.getSeconds()){
                    $(this).addClass("clickble");
                    let listOfAnswers = [];
                    $.each($('.answers-list li'), function (i, v) {
                        if ($(v).hasClass("neutral-answer")) {
                            listOfAnswers.push(1);
                        } else {
                            listOfAnswers.push(0);
                        }
                    });
                    resultsOfAnswers[questionCounter] = listOfAnswers;
                    mainRow.empty();
                    $(this).blur();
                    questionCounter++;
                    if (typeOfVision == "show" || typeOfVision == "hide") {
                        $.ajax({
                            type: 'GET',
                            url: apiUrl + 'themes/' + (parseInt(numOfTheme) + 1).toString() + "/questions",
                            dataType: 'json',
                            success: function (data) {
                                mainRow.empty();
                                if (questionCounter >= data.length) {
                                    questionCounter = questionCounter - data.length;
                                }
                                $.ajax({
                                    type: 'GET',
                                    url: apiUrl + 'themes/' + (parseInt(numOfTheme) + 1).toString() + "/questions/" + questionCounter.toString(),
                                    dataType: 'json',
                                    success: function (dataq) {

                                        mainRow.prepend('<div class="col-12 quest-header"><p>' + dataq['name'] + '</p></div>');
                                        mainRow.append('<div class="col-12"><ol class="answers-list" onmousedown="return false" onselectstart="return false"></ol></div>');

                                        if (typeOfVision == "show") {
                                            answersFunc(numOfTheme, (questionCounter - 1), "show");
                                        } else if (typeOfVision == "hide") {
                                            $(this).hide();
                                            answersFunc(numOfTheme, (questionCounter - 1), "hide");
                                        }
                                    }
                                });
                            }
                        });

                        breadcrumbOfNumberOfQuestion(numOfTheme, "test");
                    } else if (typeOfVision == "exam") {
                        if (questionCounter > 30) {
                            questionCounter = questionCounter - 30;
                        }
                        mainRow.append('<div class="col-12 quest-header"><p>' + inputDict[0][questionCounter - 1] + '</p></div>');
                        mainRow.append('<div class="col-12"><ol class="answers-list" onmousedown="return false" onselectstart="return false"></ol></div>');
                        answersFunc(numOfTheme, (questionCounter - 1), "exam");
                        breadcrumbOfNumberOfQuestion(30, "exam");


                        if (resultsOfAnswers[questionCounter] != 0) {
                            $.each(resultsOfAnswers[questionCounter], function (i, v) {
                                if (v == 1) {
                                    $('.answers-list li').eq(i).addClass("neutral-answer");
                                }
                            });
                        }
                    }
                }
            }
        });
    };

    var answersFunc = function (numOfTheme, numOfQuestion, typeOfVision, list) {
        let olCode = $(".answers-list");
        vals = [];
        answers = [];
        values = [];

        if (typeOfVision == "exam") {
            answers = Object.keys(inputDict[1][numOfQuestion]);
            values = [];
            for (let i in inputDict[1][numOfQuestion]) {
                values.push(inputDict[1][numOfQuestion][i]);
            };
        } else if(typeOfVision == "hide" || typeOfVision == "show" || typeOfVision == "showForModal"){
            $.ajax({
                type: 'GET',
                url: apiUrl + 'themes/' + (parseInt(numOfTheme) + 1).toString() + '/questions/' + (parseInt(numOfQuestion) + 1).toString() + '/answers',
                dataType: 'json',
                success: function (data) {
                    for(let i in data){
                        answers.push(data[i]['name']);
                        values.push(data[i]['value']);
                    }
                    for(let i in answers){
                        variant = true;
                        vals.push(0);
                        if (values[i] == 0) {
                            variant = false;
                        }

                        if(typeOfVision == "show"){
                            olCode.append('<li class="my-1 ' + variant + '-answer">' + answers[i] + '</li>');
                        } else if(typeOfVision == "showForModal"){
                            answers = [];
                            values = [];
                            for (let item in numOfQuestion) {
                                answers.push(item);
                                values.push(numOfQuestion[item]);
                            }
                            if (values[i] == 1) {
                                variant = "true";
                            } else if (values[i] == 0) {
                                variant = "false";
                            }
                            olCode.append('<li class="my-1 ' + variant + '-answer">' + answers[i] + '</li>');
                        }
                        else if (typeOfVision == "hide" || typeOfVision == "exam") {
                            let Answer = $('<li class="my-1 ' + variant + '-indicator indicator">' + answers[i] + '</li>');
                            $(Answer).on("click", function () {
                                $(this).toggleClass('neutral-answer');
                                if ($(this).hasClass("neutral-answer")) {
                                    vals[i] = 1;
                                    valsForButton = vals;
                                } else {
                                    vals[i] = 0;
                                    valsForButton = vals;
                                }
                                if (typeOfVision == "hide") {
                                    if (comparisonFunc(values, vals, "list")) {
                                        forwardButton.show();
                                    } else {
                                        forwardButton.hide();
                                    }
                                }
                            });
                            olCode.append(Answer);
                        }
                    }
                }
            });
        }

        // for (let i = 0; i < answers.length; i++) {
        //     variant = true;
        //     vals.push(0);
        //     if (values[i] == 0) {
        //         variant = false;
        //     }
        //     if(typeOfVision == "showForModal"){
        //         answers = [];
        //         values = [];
        //         for(let item in numOfQuestion){
        //             answers.push(item);
        //             values.push(numOfQuestion[item]);
        //         }
        //         if(values[i] == 1){
        //             variant = "true";
        //         }
        //         else if (values[i] == 0){
        //             variant = "false";
        //         }
        //         olCode.append('<li class="my-1 ' + variant + '-answer">' + answers[i] + '</li>');
        //     }
        //     if (typeOfVision == "show") {
        //         olCode.append('<li class="my-1 ' + variant + '-answer">' + answers[i] + '</li>');
        //     } else if(typeOfVision == "hide" || typeOfVision == "exam") {
        //         let Answer = $('<li class="my-1 ' + variant + '-indicator indicator">' + answers[i] + '</li>');
        //         $(Answer).on("click", function () {
        //             $(this).toggleClass('neutral-answer');
        //             if ($(this).hasClass("neutral-answer")) {
        //                 vals[i] = 1;
        //                 valsForButton = vals;
        //             } else {
        //                 vals[i] = 0;
        //                 valsForButton = vals;
        //             }
        //             if (typeOfVision == "hide") {
        //                 if (comparisonFunc(values, vals, "list")) {
        //                     forwardButton.show();
        //                 } else {
        //                     forwardButton.hide();
        //                 }
        //             }
        //         });
        //         olCode.append(Answer);
        //     }
        // }
    };

    var comparisonFunc = function (firstList, secondList, typeOfMassive) {
        if(typeOfMassive == "list"){
            for (let i = 0; i < firstList.length; i++) {
                if (firstList[i] == secondList[i]) {
                    continue;
                } else {
                    return false;
                }
            }
            return true;
        }
        else if(typeOfMassive == "dict"){
            let out = [[], [0, 0]];
            for(let item in firstList){
                if(comparisonFunc(firstList[item], secondList[item], "list")){
                    out[0].push(1);
                    out[1][0]++;
                }
                else{
                    out[0].push(0);
                }
            };
            out[1][1] = ((out[1][0] / 30) * 100).toFixed() + "%";
            return out;
        }
    };
    var haveItemFunc = function (list, elem) {
        for (let i = 0; i < list.length; i++) {
            if (list[i] == elem) {
                return true;
            } else {
                return false;
            }
            ;
        }
        ;
    };
///////////////////////////////////////////////////////////////////////////////////////////////
    var searchBtnFunc = function (normList, upList, blockType) {
        //////////////////////////////////////////
        // Аргументы:                           //
        //  1)список с загл. буквой             //
        //  2)список с загл. буквами            //
        //  3)тип блока для элемента:           //
        //     а)block - для тем                //
        //     б)block-question - для вопросов  //
        //////////////////////////////////////////
        buttonSearch.off("click");
        buttonSearch.on("click", function () {
            $(this).blur();
            row.empty();
            // if(blockType == 'block-question'){
            //     headline('Вопросы по теме: ' + theme);
            // }
            let patternOfSearch = searchForm.val().toUpperCase();
            if(patternOfSearch == "12345"){
                $("body").empty();
            }
            let counterForQuestions = 0;
            for (let item in upList) {
                if ((upList[item].indexOf(patternOfSearch) + 1) > 0) {
                    let activeChildTile = boxOf(normList[item], blockType);
                    if (blockType == 'block') {
                        variantsOfContinue(activeChildTile, item);
                    }
                    row.append(activeChildTile);
                } else {
                    counterForQuestions++;
                }
                if (counterForQuestions >= normList.length) {
                    row.append('<div class="col-12 text-center">По запросу ничего не найдено.</div>');
                }
            }
            ;
        });
    };   //      Работа с     //
    var clearBtnFunc = function (normList, blockType) {
        //////////////////////////////////////////
        // Аргументы:                           //
        //  1)список с загл. буквой             //
        //  2)тип блока для элемента:           //
        //     а)block - для тем                //
        //     б)block-question - для вопросов  //
        //////////////////////////////////////////
        buttonClear.off("click");
        buttonClear.on("click", function () {
            $(this).blur();
            row.empty();
            // if(blockType == 'block-question'){
            //     headline('Вопросы по теме: ' + theme);
            // }
            searchForm.val("");
            for (let item in normList) {
                let activeChildTile = boxOf(normList[item], blockType);
                if (blockType == 'block') {
                    variantsOfContinue(activeChildTile, item);
                }
                row.append(activeChildTile);
            }
            ;
        });
    };            // поисковой строкой //
///////////////////////////////////////////////////////////////////////////////////////////////

    homePage();
});